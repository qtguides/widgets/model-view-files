#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    modeloCarpetas = new QFileSystemModel(this);
    modeloCarpetas->setRootPath(QDir::currentPath());
    modeloCarpetas->setFilter(QDir::NoDotAndDotDot | QDir::AllDirs);
    modeloCarpetas->sort(0, Qt::AscendingOrder);
    ui->treeViewCarpetas->setModel(modeloCarpetas);

    modeloArchivos = new QFileSystemModel(this);
    modeloArchivos->setRootPath(QDir::currentPath());
    modeloArchivos->setFilter(QDir::NoDotAndDotDot | QDir::Files);
    modeloArchivos->sort(0, Qt::AscendingOrder);
    ui->listViewArchivos->setModel(modeloArchivos);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_treeViewCarpetas_clicked(const QModelIndex &index)
{
    QString path = modeloCarpetas->fileInfo(index).absoluteFilePath();
    QModelIndex modelIndex = modeloArchivos->setRootPath(path);
    ui->listViewArchivos->setRootIndex(modelIndex);
}
