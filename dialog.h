#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QFileSystemModel>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();

private slots:
    void on_treeViewCarpetas_clicked(const QModelIndex &index);

private:
    Ui::Dialog *ui;

    // Modelo que representa los datos. Los datos seran las carpetas en el disco.
    // Nota: ui->treeViewCarpetas es el encargador de dibujar en pantalla este modelo.
    QFileSystemModel * modeloCarpetas;

    // Modelo que representa los datos. Los datos seran loss archivos en la carpeta.
    // Nota: ui->listViewArchivos es el encargador de dibujar en pantalla este modelo.
    QFileSystemModel * modeloArchivos;
};

#endif // DIALOG_H
